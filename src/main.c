/*
 * Copyright (c) 2019 Manivannan Sadhasivam
 *
 * SPDX-License-Identifier: Apache-2.0
 */

#include <zephyr/device.h>
#include <zephyr/drivers/lora.h>
#include <errno.h>
#include <zephyr/sys/util.h>
#include <zephyr/kernel.h>


void lora_receive_cb(const struct device *dev, uint8_t *data, uint16_t len, int16_t rssi, int8_t snr)
{
	if(len==0)return;
	printk("Received data: dev: %s, len: %d, RSSI: %d dBm, SNR:%d dBm, data:%s\n", dev->name, len, rssi, snr, data);
}

void main(void)
{
	printk("Welcome to hello_lora_receive\n");
	printk("Wait 5 seconds...\n");
	k_msleep(5*1000);

	const struct device *const lora_dev = DEVICE_DT_GET(DT_ALIAS(lora0));
	if (!device_is_ready(lora_dev)) {
		printk("%s Device not ready", lora_dev->name);
		return 0;
	}

	struct lora_modem_config config;
	config.frequency = 865100000;
	config.bandwidth = BW_125_KHZ;
	config.datarate = SF_8;
	config.preamble_len = 8;
	config.coding_rate = CR_4_5;
	config.iq_inverted = false;
	config.public_network = false;
	config.tx_power = 14;
	config.tx = false;

	if(lora_config(lora_dev, &config)< 0) {
		printk("Lora configuration failed... program termination\n");
		return 0;
	}

	/*
	printk("Synchronous reception\n");
	for (int i = 0; i < 4; i++) {
		printk("Wait for data...\n");
		// Block until data arrives
		len = lora_recv(lora_dev, data, MAX_DATA_LEN, K_FOREVER, &rssi, &snr);
		if (len < 0) {
			LOG_ERR("LoRa receive failed");
			return;
		}
		printk("Received data: len: %d, RSSI: %d dBm, SNR:%d dBm\n", len, rssi, snr);
	}
	*/
	
	/* Enable asynchronous reception */
	printk("Enable asynchronous reception\n");
	lora_recv_async(lora_dev, lora_receive_cb);
	while(1){
		k_msleep(5000);
		printk("Im still alive\n");
	}
	return;
}
